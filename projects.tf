resource "gitlab_project" "README" {
  name                   = "README"
  description            = "Overview of GitOps-Demo Group"
  default_branch         = "master"
  namespace_id           = data.gitlab_group.gitops-demo.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "my-aspdotnet-app1" {
  name                   = "My ASP .Net App1"
  description            = "AutoDevOps with Dockerfile and template overides."
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "my-spring-app2" {
  name                   = "My Spring App2"
  description            = "AutoDevOps with Dockerfile and default template"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "my-ruby-app3" {
  name                   = "My Ruby App3"
  description            = "AutoDevOps with default template, no Dockerfile"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "my-python-app4" {
  name                   = "My Python App4"
  description            = "AutoDevOps with Dockerfile and Helm chart"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "templates" {
  name             = "templates"
  default_branch   = "master"
  namespace_id     = gitlab_group.infra.id
  visibility_level = "public"
}
resource "gitlab_project" "aws" {
  name                   = "aws"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "gcp" {
  name                   = "gcp"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "azure" {
  name                   = "azure"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "openshift" {
  name                   = "openshift"
  default_branch         = "master"
  namespace_id           = gitlab_group.infra.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "cluster-management" {
  name                   = "cluster-management"
  default_branch         = "master"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}